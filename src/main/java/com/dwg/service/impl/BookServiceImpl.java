package com.dwg.service.impl;

import com.dwg.dao.impl.BookDao;


public class BookServiceImpl implements BookService{

    private BookDao bookDao;
    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    public void save() {
        bookDao.save();
    }
}
