package com.dwg.controller;

public class User1 {
    @Override
    public String toString() {
        return "User1{" +
                "name='" + name + '\'' +
                '}';
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User1() {
    }

    public User1(String name) {
        this.name = name;
    }
}
