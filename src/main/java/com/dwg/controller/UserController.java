package com.dwg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

@Controller
public class UserController {

    @RequestMapping(value="index")
    public String index(){
        System.out.println("get in index");
        return "test";
    }

    @RequestMapping("/que2")
    @ResponseBody
    public void save1(@RequestBody List<User1> userList) throws IOException {
        System.out.println(userList);
    }

    @RequestMapping("/que")
    @ResponseBody
    public User1 save2(User1 user1) throws IOException {
        User1 user2 = new User1();
        user2.setName("sssss");
        return user2;
    }
}
